// --- Soal 1 ---

const persegipanjang = (panjang, lebar) => {
    const luas = (panjang, lebar) => {
        let p = panjang;
        let l = lebar;
        return p*l;
    };
    const keliling = (panjang, lebar) => {
        const p = panjang;
        const l = lebar;
        return 2*(p+l);
    };
    console.log("Simulasi Rumus Luas dan Keliling Persegi Panjang :");
    console.log(`JIka Panjangnya ${panjang} dan Lebarnya ${lebar}`);
    const cekRumus = `Maka Luasnya ${luas(panjang, lebar)} dan Kelilingnya ${keliling(panjang, lebar)}`;
    return cekRumus;
};
console.log(persegipanjang(4, 6));


// --- Soal 2 ---

const newFunction = (firstName, lastName) => {
    const lengkap = `${firstName} ${lastName}`;
    return {
        fullName: () => {
            console.log(lengkap);
        },
    };
};
//Driver Code
newFunction("William", "Imoh").fullName();


// --- Soal 3 ---

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
};
const { firstName, lastName, address, hobby } = newObject;
// Driver code
console.log(firstName, lastName, address, hobby);


// --- Soal 4 ---

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// const combined = west.concat(east);
const combined = [...west, ...east];

//Driver Code
console.log(combined);


// --- Soal 5 ---

const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(before);